//
//  SelectorTextField.swift
//  SelectorTextField
//
//  Copyright © 2019 john. All rights reserved.
//

import UIKit

class SelectorTextField: UIView {
    
    private enum Status{
        case opened
        case closed
    }
    
    private var tableView: UITableView!
    private var textField: UITextField!
    private var dropDownArrow:UIImageView!
    private var containerView:UIView!
    private var textViewGesture:UITapGestureRecognizer!
    private var dropDownGesture:UITapGestureRecognizer!
    private var tableHeightConstraint:NSLayoutConstraint!
    private var status:Status!
    
    private var tableViewData:[String] = ["Basketball", "Football", "Soccer", "Baseball", "Hockey" ]
    var cellHeight:CGFloat = 40.0
    
    var initialText:String = "Select a Sport"
    var dividerThickness:CGFloat = 5.0
    var dividerColor:UIColor = UIColor.init(hexString: "ebeced")
    var arrowColor:UIColor = UIColor.init(hexString: "b5b5b5")
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        
        self.bounds.size = CGSize(width: self.frame.width, height: self.bounds.height + (CGFloat(tableViewData.count) * cellHeight))
        
        initializeComponents()
        positionComponents()
        
        textViewGesture = UITapGestureRecognizer.init(target: self, action:  #selector(triggerTable))
        textField.addGestureRecognizer(textViewGesture)
        dropDownGesture = UITapGestureRecognizer.init(target: self, action:  #selector(triggerTable))
        dropDownArrow.addGestureRecognizer(dropDownGesture)
        
        status = Status.closed
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

//MARK:- Initializers
extension SelectorTextField{
    
    private func initializeComponents(){
        
        
        // Tableview
        tableView = UITableView(frame: CGRect.zero)
        tableView.register(SelectorCell.self, forCellReuseIdentifier: "selectorcell")
        tableView.bounces = false
        tableView.backgroundColor = UIColor.init(hexString: "ebeced")
        tableView.separatorInset = UIEdgeInsets.zero
        tableView.separatorColor = UIColor.clear
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView(frame:CGRect.init(x: 0, y: 0, width: 1, height: 1))
        
        self.addSubview(tableView)
        
        //Container
        containerView = UIView(frame:.zero)
       
        
        //TextField
        textField = UITextField(frame: .zero)
        textField.text = initialText
        // textField.backgroundColor = UIColor.blue
        textField.font = UIFont.systemFont(ofSize: 18, weight: UIFont.Weight.medium)
        containerView.addSubview(textField)
        
        //Dropdown Arrow
        dropDownArrow = UIImageView.init(frame: .zero)
        dropDownArrow.image = UIImage(named: "dropdown")
        dropDownArrow.isUserInteractionEnabled = true
        dropDownArrow.tintColor = arrowColor
        containerView.addSubview(dropDownArrow)
        
        self.addSubview(containerView)
    }
    
    
    private func positionComponents(){
        
        //Container
        containerView.translatesAutoresizingMaskIntoConstraints = false
        containerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant:10.0).isActive = true
        containerView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        containerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant:-10.0).isActive = true
        containerView.heightAnchor.constraint(equalToConstant: cellHeight).isActive = true
        
        
        //Textfield
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant:10.0).isActive = true
        textField.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        textField.trailingAnchor.constraint(equalTo: dropDownArrow.leadingAnchor).isActive = true
        textField.heightAnchor.constraint(equalToConstant: cellHeight).isActive = true
        
        
        //Dropdown
        let arrowWidthHeight = cellHeight/2
        dropDownArrow.translatesAutoresizingMaskIntoConstraints = false
        dropDownArrow.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant:-10.0).isActive = true
        dropDownArrow.topAnchor.constraint(equalTo: containerView.topAnchor, constant:arrowWidthHeight/2).isActive = true
        dropDownArrow.heightAnchor.constraint(equalToConstant: arrowWidthHeight).isActive = true
        dropDownArrow.widthAnchor.constraint(equalToConstant: arrowWidthHeight).isActive = true
        
        //Tableview
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.leadingAnchor.constraint(equalTo: leadingAnchor, constant:15.0).isActive = true
        tableView.trailingAnchor.constraint(equalTo: trailingAnchor, constant:-15.0).isActive = true
        tableView.topAnchor.constraint(equalTo: containerView.bottomAnchor).isActive = true
        
        // Round the edges of the dropdown table
        tableView.clipsToBounds = true
        tableView.layer.cornerRadius = 10
        tableView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        
        tableHeightConstraint = tableView.heightAnchor.constraint(equalToConstant: 0.0)
        tableHeightConstraint.isActive = true
        
        //Add border to container
        containerView.layoutIfNeeded()
        containerView.layer.addBorder(edge: .bottom, color: dividerColor, thickness: dividerThickness)
        
        
    }
    
}


//MARK:- TableViewDelegate & TableViewDataSource
extension SelectorTextField: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if status == .closed{
            return 0
        }else{
            return tableViewData.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "selectorcell") as? SelectorCell ?? SelectorCell()
        
        cell.textLabel?.text = self.tableViewData[indexPath.row]
        cell.backgroundColor = UIColor.init(hexString: "ebeced")
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
    
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return cellHeight
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.textField.text = tableViewData[indexPath.row]
        triggerTable()
        
    }
    
    
}

extension SelectorTextField{
    
    @objc private func triggerTable(){
        
        if status == .closed{
            status = .opened
            
            UIView.animate(withDuration: 0.7, delay:0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseInOut, animations:{
                self.tableHeightConstraint.constant +=  self.cellHeight * CGFloat(self.tableViewData.count)
                self.dropDownArrow.transform = CGAffineTransform(rotationAngle: .pi)
                self.layoutIfNeeded()
                
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .milliseconds(300), execute: {
                    self.tableView.separatorColor = UIColor.darkGray
                    self.tableView.reloadData()
                })
            }){_ in
                //Remove DispatchQueue above and uncomment this when changing aniumation
                //to not use springVelocity and Damping
                //self.tableView.separatorColor = UIColor.darkGray
                //self.tableView.reloadData()
            }
            
            
        }else{
            
            status = .closed
            self.tableView.reloadData()
            UIView.animate(withDuration: 0.7, delay:0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .curveEaseInOut, animations:{
                
                self.tableHeightConstraint.constant -=  self.cellHeight * CGFloat(self.tableViewData.count)
                self.dropDownArrow.transform = CGAffineTransform(rotationAngle: .pi - 3.14159)
                self.tableView.separatorColor = UIColor.clear
                self.layoutIfNeeded()
                
            })
        }
        
    }
}


//MARK:- CALayer Extension
// From Stackoverflow:
// https://stackoverflow.com/questions/17355280/how-to-add-a-border-just-on-the-top-side-of-a-uiview
extension CALayer {
    
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        
        let border = CALayer()
        
        switch edge {
        case .top:
            border.frame = CGRect(x: 0, y: 0, width: frame.width, height: thickness)
        case .bottom:
            border.frame = CGRect(x: 0, y: frame.height - thickness, width: frame.width, height: thickness)
        case .left:
            border.frame = CGRect(x: 0, y: 0, width: thickness, height: frame.height)
        case .right:
            border.frame = CGRect(x: frame.width - thickness, y: 0, width: thickness, height: frame.height)
        default:
            break
        }
        
        border.backgroundColor = color.cgColor;
        
        addSublayer(border)
    }
}


extension UIColor{
    
    convenience init(hexString:String) {

        let hexString:String = hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let scanner = Scanner(string: hexString)
        
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        
        var color:UInt32 = 0
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        
        self.init(red:red, green:green, blue:blue, alpha:1)
    }
}
